import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import * as awsx from "@pulumi/awsx";

// Create an AWS resource (S3 Bucket)
const bucket = new aws.s3.Bucket("lb-sample");

// Export the name of the bucket
export const bucketName = bucket.id;

const name = "lb-sample";

const awsx_network = new awsx.Network(name);
const aws_vpc_main = aws.ec2.Vpc.get(name, awsx_network.vpcId);

const lbSecurityGroup = new aws.ec2.SecurityGroup("loadbalancer", {
  description: "loadbalancer",
  egress: [
    {
      cidrBlocks: ["0.0.0.0/0"],
      fromPort: 0,
      protocol: "-1",
      toPort: 0
    }
  ],
  ingress: [
    {
      cidrBlocks: ["0.0.0.0/0"],
      protocol: "tcp",
      fromPort: 80,
      toPort: 80
    },
    {
      cidrBlocks: ["0.0.0.0/0"],
      protocol: "tcp",
      fromPort: 443,
      toPort: 443
    }
  ],
  vpcId: aws_vpc_main.id
});

const frontEndLoadBalancer = new aws.elasticloadbalancingv2.LoadBalancer(
  "lb-sample",
  { subnets: awsx_network.subnetIds, securityGroups: [lbSecurityGroup.id] }
);

const frontEndTargetGroup = new aws.elasticloadbalancingv2.TargetGroup(
  "lb-sample",
  { port: 80, protocol: "HTTP", vpcId: aws_vpc_main.id }
);

const zoneDomainName = "1810.sandbox.kytiken.com.";
const aws_route53_zone_primary = aws.route53.Zone.get(
  zoneDomainName,
  "ZPKM3KL38RM53"
);

aws_route53_zone_primary.name.apply(() => {
  const subDomain1 = "lb-sample-1";
  const domainName1 = `${subDomain1}.${zoneDomainName}`;

  const cert1 = new aws.acm.Certificate("cert1", {
    domainName: domainName1,
    validationMethod: "DNS"
  });

  cert1.domainValidationOptions.apply(domainValidationOptions => {
    const certValidation1 = new aws.route53.Record("cert_validation1", {
      name: domainValidationOptions[0].resourceRecordName.toString(),
      records: [domainValidationOptions[0].resourceRecordValue],
      ttl: 60,
      type: domainValidationOptions[0].resourceRecordType,
      zoneId: aws_route53_zone_primary.id
    });
  });

  new aws.route53.Record(subDomain1, {
    name: domainName1,
    records: [frontEndLoadBalancer.dnsName],
    ttl: 60,
    type: "CNAME",
    zoneId: aws_route53_zone_primary.zoneId
  });

  cert1.arn.apply(certArn1 => {
    const frontEndListener = new aws.elasticloadbalancingv2.Listener(
      "front_end",
      {
        certificateArn: cert1.arn,
        defaultActions: [
          {
            targetGroupArn: frontEndTargetGroup.arn,
            type: "forward"
          }
        ],
        loadBalancerArn: frontEndLoadBalancer.arn,
        port: 443,
        protocol: "HTTPS",
        sslPolicy: "ELBSecurityPolicy-2016-08"
      }
    );
  });

  const subDomain2 = "lb-sample-2";
  const domainName2 = `${subDomain2}.${zoneDomainName}`;

  const cert2 = new aws.acm.Certificate("cert2", {
    domainName: domainName2,
    validationMethod: "DNS"
  });

  cert2.domainValidationOptions.apply(domainValidationOptions => {
    const certValidation2 = new aws.route53.Record("cert_validation2", {
      name: domainValidationOptions[0].resourceRecordName.toString(),
      records: [domainValidationOptions[0].resourceRecordValue],
      ttl: 60,
      type: domainValidationOptions[0].resourceRecordType,
      zoneId: aws_route53_zone_primary.id
    });
  });

  new aws.route53.Record(subDomain2, {
    name: domainName2,
    records: [frontEndLoadBalancer.dnsName],
    ttl: 60,
    type: "CNAME",
    zoneId: aws_route53_zone_primary.zoneId
  });
});
